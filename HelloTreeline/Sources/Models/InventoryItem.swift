//
//  InventoryItem.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

typealias InventoryItemId = String

struct InventoryItem: Decodable {
    let id: InventoryItemId
    let title: String
    let description: String
    let color: String
    let available: Int
    let cost: Float?
}
