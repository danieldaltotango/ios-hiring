//
//  DetailsInteractor.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

protocol DetailsInteractorDelegate: AnyObject {
    func itemDidLoad(item: InventoryItem)
}

class DetailsInteractor {

    weak var delegate: DetailsInteractorDelegate?
    
    private let itemRepository: InventoryItemRepository

    init(itemRepository: InventoryItemRepository = DefaultInventoryItemRepository.shared) {
        self.itemRepository = itemRepository
    }

    func getItemSales(objectId: String) -> Int {
        return itemRepository.getSales(objectId: objectId)
    }

    func increaseItemSales(objectId: String) -> String? {
        let sales = itemRepository.getSales(objectId: objectId)
        if let item = itemRepository.get(objectId: objectId),
           item.available-sales <= 0 {
            return "Out of stock!!!"
        }
        itemRepository.increaseSales(objectId: objectId)
        return nil
    }

    func decreaseItemSales(objectId: String) {
        itemRepository.decreaseSales(objectId: objectId)
    }
    
    func loadItem(objectId: String) {
        guard let item = itemRepository.get(objectId: objectId) else { return }
        delegate?.itemDidLoad(item: item)
    }
}
