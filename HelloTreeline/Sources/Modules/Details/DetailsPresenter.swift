//
//  DetailsPresenter.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation
import UIKit

class DetailsPresenter: DetailsInteractorDelegate {

    private let router: DetailsRouter
    private let interactor: DetailsInteractor
    private weak var view: DetailsViewController!
    
    private let objectId: String

    init(view: DetailsViewController,
         interactor: DetailsInteractor,
         router: DetailsRouter,
         objectId: String) {
        
        self.view = view
        self.interactor = interactor
        self.router = router

        self.objectId = objectId
        
        self.interactor.delegate = self
    }
    
    func viewDidLoad() {
        interactor.loadItem(objectId: objectId)

        let sales = interactor.getItemSales(objectId: objectId)
        view.soldLabelTitle = "Today sales/returns: \(sales) units"
    }
    
    func itemDidLoad(item: InventoryItem) {
        view.navBarTitle = item.title
        view.idLabelTitle = "ID: \(item.id)"
        view.titleLabelTitle = "Title: \(item.title)"
        view.desciptionLabelTitle = "Description: \(item.description)"
        view.colorLabelTitle = "Color: \(item.color)"
        view.costLabelTitle = item.cost != nil ? "Cost: \(item.cost!)" : "Cost: Not Available"

        let sales = interactor.getItemSales(objectId: objectId)
        view.availableLabelTitle = "Available: \(item.available-sales) units"
        view.soldLabelTitle = "Today sales/returns: \(sales) units"
    }

    func increaseSales() {
        if let errorStr = interactor.increaseItemSales(objectId: objectId) {
            view.showError(error: errorStr)
        }
        interactor.loadItem(objectId: objectId)
    }

    func decreaseSales() {
        interactor.decreaseItemSales(objectId: objectId)
        interactor.loadItem(objectId: objectId)
    }
}
