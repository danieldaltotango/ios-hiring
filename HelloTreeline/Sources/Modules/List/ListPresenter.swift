//
//  ListPresenter.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

class ListPresenter: ListInteractorDelegate {

    private let router: ListRouter
    private let interactor: ListInteractor
    private weak var view: ListViewController!
    
    private var listItems: [ListItem] = []
    
    var itemCount: Int { listItems.count }

    init(view: ListViewController, interactor: ListInteractor, router: ListRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router

        self.interactor.delegate = self
    }
    
    func viewWillAppear() {
        interactor.loadItems()
    }
    
    func listItem(at indexPath: IndexPath) -> ListItem {
        listItems[indexPath.row]
    }
    
    func listItemSelected(objectId: String) {
        router.routeToDetails(objectId: objectId)
    }
    
    func itemsDidLoad(items: [InventoryItem]) {
        self.listItems = items.map { .init(id: $0.id, title: "\($0.title) - \($0.color)") }
        self.view.reloadList()
        self.view.reportButtonEnabled = true
    }

    func buildReportEmailController() -> (recipients: [String], subject: String, htmlBody: String) {
        let today = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM y"
        let dateKey = formatter.string(from: today)
        var report: String = "<h2>Daily Report - \(dateKey)</h2><table border=\"1\"><tr><td><b>Item</b></td><td><b>Sales</b></td></tr>"
        let sales = interactor.getDailySales()
        sales.keys.forEach({ objectId in
            if let item = self.listItems.first(where: {$0.id == objectId}) {
                report.append("<tr><td>\(item.id)<br/>\(item.title)</td><td>\(sales[objectId]!)</td></tr>")
            }
        })
        report.append("</table>")
        return (["bossman@bosscompany.com"],"Daily Sales Report",report)
    }
}
