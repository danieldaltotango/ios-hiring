//
//  ListViewContoller.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

protocol ListViewController: AnyObject {
    var reportButtonEnabled: Bool { get set }
    func reloadList()
}

class ListDefaultViewController: UIViewController, ListViewController {

    var reportButtonEnabled: Bool {
        get { reportButton.isEnabled }
        set {
            reportButton.isEnabled = newValue
            reportButton.alpha = newValue ? 1 : 0.5
        }
    }

    @IBOutlet weak var reportButton: UIButton!
    @IBOutlet var reportHeaderView: UIView!

    static func build() -> ListDefaultViewController {
        let viewController = UIStoryboard.main.instantiateViewController(of: ListDefaultViewController.self)!
        let router = ListDefaultRouter(viewController: viewController)
        let interactor = ListInteractor()

        viewController.presenter = ListPresenter(view: viewController, interactor: interactor, router: router)

        return viewController
    }

    private var presenter: ListPresenter!

    @IBOutlet weak var tableView: UITableView!
    
    private let CellIdentifier = "ListCellIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CellIdentifier)
        tableView.tableHeaderView = reportHeaderView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter.viewWillAppear()
    }
    
    func reloadList() {
        tableView.reloadData()
    }

    @IBAction func sendDailySales() {
        let mailData = presenter.buildReportEmailController()
        sendEmail(recipients: mailData.recipients,
                  subject: mailData.subject,
                  htmlBody: mailData.htmlBody)
    }

    func sendEmail(recipients: [String], subject: String, htmlBody: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setSubject(subject)
            mail.setToRecipients(recipients)
            mail.setMessageBody(htmlBody, isHTML: true)
            present(mail, animated: true)
        } else {
            // show failure alert
            showError(error: "Can not send emails from this device.")
        }
    }

    func showError(error: String) {
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Ok", style: .cancel)
        alert.addAction(cancel)
        self.present(alert, animated: true)
    }
}

extension ListDefaultViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

extension ListDefaultViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.itemCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier)!
        cell.textLabel?.text = presenter.listItem(at: indexPath).title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objectId = presenter.listItem(at: indexPath).id
        presenter.listItemSelected(objectId: objectId)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
