//
//  InventoryItemRepository.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

protocol InventoryItemRepository {
    func getAll(completion: @escaping (Result<[InventoryItem], Error>) -> Void)
    func get(objectId: InventoryItemId) -> InventoryItem?

    func getSales(objectId: InventoryItemId) -> Int
    func getAllSales() -> [InventoryItemId:Int]
    func increaseSales(objectId: InventoryItemId)
    func decreaseSales(objectId: InventoryItemId)
}

class DefaultInventoryItemRepository: InventoryItemRepository {
    
    static let shared: InventoryItemRepository = DefaultInventoryItemRepository()
    
    private let apiClient: APIClient
    
    private var items: [InventoryItem] = []

    private var todaySales: [InventoryItemId:Int] = [:]

    private var dateKey: String {
        let today = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM y"
        let dateKey = formatter.string(from: today)
        return dateKey
    }
    
    init(apiClient: APIClient = DefaultAPIClient.shared) {
        self.apiClient = apiClient

        // load local data for sales
        if let sales = UserDefaults.standard.dictionary(forKey: dateKey) as? [InventoryItemId:Int] {
            todaySales = sales
        }
    }

    private func saveSales() {
        UserDefaults.standard.setValue(todaySales, forKey: dateKey)
        UserDefaults.standard.synchronize()
    }
    
    func getAll(completion: @escaping (Result<[InventoryItem], Error>) -> Void) {
        apiClient.get(endpoint: "getInventory", type: [InventoryItem].self) { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let items):
                    self.items = items.sorted(by: { item1, item2 in
                        if item1.title == item2.title {
                            return item1.color < item2.color
                        } else {
                            return item1.title < item2.title
                        }
                    })
                    completion(.success(self.items))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    func get(objectId: String) -> InventoryItem? {
        items.first(where: { $0.id == objectId })
    }

    func getSales(objectId: InventoryItemId) -> Int {
        return todaySales[objectId] ?? 0
    }

    func getAllSales() -> [InventoryItemId:Int] {
        return todaySales
    }

    func increaseSales(objectId: InventoryItemId) {
        var sales = todaySales[objectId] ?? 0
        sales += 1
        todaySales[objectId] = sales
        saveSales()
    }

    func decreaseSales(objectId: InventoryItemId) {
        var sales = todaySales[objectId] ?? 0
        sales -= 1
        todaySales[objectId] = sales
        saveSales()
    }
}
