//
//  MockInventoryItemRepository.swift
//  HelloTreelineTests
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation
@testable import HelloTreeline

class MockInventoryItemRepository: InventoryItemRepository {
    func getSales(objectId: InventoryItemId) -> Int {
        return 0
    }

    func getAllSales() -> [InventoryItemId : Int] {
        return [:]
    }

    func increaseSales(objectId: InventoryItemId) {

    }

    func decreaseSales(objectId: InventoryItemId) {
        
    }

    
    var items: [InventoryItem] = []
    
    func getAll(completion: @escaping (Result<[InventoryItem], Error>) -> Void) {
        completion(.success(items))
    }
    
    func get(objectId: String) -> InventoryItem? {
        items.first(where: { $0.id == objectId })
    }
}
