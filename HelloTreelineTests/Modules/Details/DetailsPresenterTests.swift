//
//  DetailsPresenterTests.swift
//  HelloTreelineTests
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import XCTest
import os
@testable import HelloTreeline

class DetailsPresenterTests: XCTestCase {
    
    private var presenter: DetailsPresenter!
    private var view: MockViewController!
    
    private let inventoryItem  = InventoryItem(id: "test-id", title: "title", description: "description", color: "color", available: 100, cost: 9.99)

    override func setUpWithError() throws {
        
        self.view = MockViewController()
        
        let itemRepository = MockInventoryItemRepository()
        itemRepository.items = [inventoryItem]
        let interactor = DetailsInteractor(itemRepository: itemRepository)
        self.presenter = DetailsPresenter(view: view, interactor: interactor, router: StubRouter(), objectId: inventoryItem.id)
    }

    func testViewDidLoadToPopulateAllLabels() throws {
        
        // before
        XCTAssertNil(view.navBarTitle)
        XCTAssertNil(view.idLabelTitle)
        XCTAssertNil(view.titleLabelTitle)
        XCTAssertNil(view.desciptionLabelTitle)
        XCTAssertNil(view.colorLabelTitle)
        XCTAssertNil(view.costLabelTitle)
        
        // when
        presenter.viewDidLoad()
        
        // then
        XCTAssertEqual(view.navBarTitle, "\(inventoryItem.title)")
        XCTAssertEqual(view.idLabelTitle, "ID: \(inventoryItem.id)")
        XCTAssertEqual(view.titleLabelTitle, "Title: \(inventoryItem.title)")
        XCTAssertEqual(view.desciptionLabelTitle, "Description: \(inventoryItem.description)")
        XCTAssertEqual(view.colorLabelTitle, "Color: \(inventoryItem.color)")
        XCTAssertEqual(view.costLabelTitle, "Cost: \(inventoryItem.cost ?? 0)")
    }
}

fileprivate class MockViewController: DetailsViewController {
    var availableLabelTitle: String?

    var soldLabelTitle: String?

    func showError(error: String) {
        os_log("%@",error)
    }
    
    var navBarTitle: String?
    
    var idLabelTitle: String?
    
    var titleLabelTitle: String?
    
    var desciptionLabelTitle: String?
    
    var colorLabelTitle: String?
    
    var costLabelTitle: String?
}

fileprivate class StubRouter: DetailsRouter { }
